package com;

import com.controller.Controller;
import com.controller.Notification;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;


/**
 * the Main class acts as the bootstrapper and nothing else,
 * this appears to be common practice: https://stackoverflow.com/questions/732151/java-main-method-good-coding-style
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            // Note: InvocationTargetException usually means that there is a symbol collision (i.e. use namespaces!)
            URL resource = getClass().getClassLoader().getResource("View.fxml");
            assert resource != null;

            // Note: this is necessary when you have a controller with constructor args
//        FXMLLoader loader = new FXMLLoader(resource);
//        loader.setController(new Controller(primaryStage, ""));
//        Parent root = loader.load();
            Parent root = FXMLLoader.load(resource);

            Notification.setPrimaryStage(primaryStage);
            primaryStage.setResizable(false);

            // this links resource file to code
            primaryStage.setScene(new Scene(root, 600, 400));
            primaryStage.setTitle("Activity Timer");
            primaryStage.show();
            primaryStage.setOnCloseRequest(event -> System.exit(0)); // fixes mac bug
        } catch(Throwable e) { // NOTE: this might not work here
            Controller.handleError(e);
        }
    }
}

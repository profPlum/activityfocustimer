package com.controller;

import com.model.Activity;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Dialog;
import javafx.stage.WindowEvent;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Notification implements Runnable {
    static Stage primary;

    Alarm alarm;
    String title;
    String header;
    String content;

    protected AlertType alertType = AlertType.WARNING;

    // we close any old alert when a new one is made
    static Alert lastAlert;
    protected void setLastAlert(Alert alert) {
        // no synchronization needed because
        // alerts can only be made in the main thread
        if (lastAlert != null) {
            // we don't want to trigger workspace change if close doesn't come from user
            lastAlert.setOnHidden(e -> {});
            lastAlert.close();
        }
        lastAlert = alert;
    }

    public AlertType getAlertType() {
        return alertType;
    }
    public void setAlertType(AlertType alertType) {
        this.alertType = alertType;
    }

    public static void setPrimaryStage(Stage primary) {
        Notification.primary = primary;
    }

    public Notification(String title, String header, String content) {
        this(null, title, header, content);
    }

    public Notification(Alarm alarm, String title, String header, String content) {
        this.alarm = alarm;
        this.title = title;
        this.header = header;
        this.content = content;
    }

    /**
     * sets a dialog window to always be on top (use before show())
     * @param dialog the dialog node
     */
    public static void setDialogAlwaysOnTop(Dialog dialog) {
        ((Stage) dialog.getDialogPane().getScene().getWindow()).setAlwaysOnTop(true);
    }

    public static Stage getStage(Dialog dialog) {
        return ((Stage) dialog.getDialogPane().getScene().getWindow());
    }

    @Override
    public void run() {
        Alert alert = new Alert(alertType);
        setDialogAlwaysOnTop(alert);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        if (alarm != null) alarm.play();
        setLastAlert(alert);

        // this is a clever workaround to make it so that the main window follows
        // the notification when the notification is closed
        alert.setOnHidden(e -> { this.primary.setAlwaysOnTop(true);
                                this.primary.hide(); this.primary.show();
                                this.primary.setAlwaysOnTop(false);
                                lastAlert=null;});
        alert.show();
    }

    public static class ProgressNotification extends Notification {

        // helper for self & others
        public static String getTimeRemainingStr(Activity activity, float progress) {
            float secDur = activity.getPlannedMinuteDuration() * 60;
            int secRemaining = (int) (secDur * (1.0 - progress));
            int minRemaining = secRemaining / 60;
            secRemaining = secRemaining % 60;
            return String.format("%d:%02d", minRemaining, secRemaining);
        }

        public ProgressNotification(Alarm alarm, Activity activity, float progress) {
            super(alarm, "\"" + activity.name + "\" Activity Progress Reminder",
                    String.format("Allotted activity time is: %f%% complete!\n", progress * 100),
                    String.format("You have: %s (min:sec) left...", getTimeRemainingStr(activity, progress)));
        }
    }

    public static class IdleNotification extends Notification {

        public static String getTimeStr(int seconds) {
            int minutes = seconds / 60;
            seconds = seconds % 60;
            return String.format("%d:%02d (min:sec)", minutes, seconds);
        }

        public IdleNotification(Alarm alarm, float secondsIdle) {
            super(alarm, "Nag Notification",
                    "Hey! Did you forget to use me?",
                    "It's been: " + getTimeStr((int)secondsIdle));
        }
    }

    public static class ErrorNotification extends Notification {

        // gets stack trace of a Throwable in string form (pretty printed)
        static private String getStrStackTrace(Throwable err) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            err.printStackTrace(pw);
            return sw.toString();
        }

        public ErrorNotification(Throwable err, String errMsg) {
            super("Error!", errMsg, getStrStackTrace(err));
            alertType = AlertType.ERROR;
        }

        @Override
        public void run() {
            super.run();
            lastAlert.setOnCloseRequest(event -> System.exit(1));
            // set to exit on close of error message box
        }
    }
}
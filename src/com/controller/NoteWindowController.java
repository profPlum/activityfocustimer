package com.controller;

import com.model.Model;
import com.model.SQLDataBase;
import javafx.event.Event;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.time.LocalDateTime;

public class NoteWindowController {
    public Button recordButton;
    public TextArea noteTextArea;
    public TextField titleField;

    public void recordNote(Event event) {
        assert(event.getSource()==recordButton);

        SQLDataBase db = Model.get().getDataBase();
        String queryFmt = "INSERT INTO Notes (title, text, time) VALUES (\"%s\", \"%s\", \"%s\")";
        String title = titleField.getText();//.replace("\"",""); // this is so it doesn't break out of quotations
        String note = noteTextArea.getText();//.replace("\"",""); // this is so it doesn't break out of quotations
        try {
            db.executeUpdate(String.format(queryFmt, title, note,
                    db.getDateTimeSQLString(LocalDateTime.now())));
        } catch (Throwable e) {
            Controller.handleError(e);
        }
    }
}

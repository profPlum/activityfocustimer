package com.controller;

import javafx.scene.media.AudioClip;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.Media;


public class Alarm {
    AudioClip alarmSound;
    String mediaFileName;

    private MediaPlayer loadAlarmPlayer(String mediaFileName) {
        MediaPlayer alarm = null;
        try {
            Media sound = new Media(getClass().getClassLoader().getResource(mediaFileName).toString());
            alarm = new MediaPlayer(sound);
        } catch (Throwable e) {
            Controller.handleError(e);
        }
        return alarm;
    }

    private AudioClip loadAlarmClip(String mediaFileName) {
        try {
            alarmSound = new AudioClip(getClass().getClassLoader().getResource(mediaFileName).toString());
        } catch (Throwable e) {
            Controller.handleError(e);
        }
        return alarmSound;
    }

    public Alarm(String mediaFileName) {
        this.mediaFileName = mediaFileName;
        loadAlarmClip(mediaFileName);
    }

    public void play() {
        try {
            // supposedly this fixes the muting issue (on mac)
            // https://stackoverflow.com/questions/50552921/mediaplayer-nosound-osx
            // https://stackoverflow.com/questions/50552921/AudioClip-nosound-osx
            alarmSound.stop();
            alarmSound.play();
        } catch (Throwable e) {
            Controller.handleError(e);
        }
    }
}

package com.controller;

import com.model.Activity;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class TagWindowController implements Initializable {
    public TextField tagEntryField;
    public ComboBox<ActivityItemSelector.ActivityItem> taskComboBox;
    public FlowPane tagPane;
    public TextField magnitudeField;
    public CheckBox showExtensionsBox;

    public ActivityItemSelector taskSelector;

    public TagWindowController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        taskSelector = new ActivityItemSelector(taskComboBox, showExtensionsBox, 10);
        updateTagButtons();
    }

    public void onAddTag() {
        addTag(tagEntryField.getText(), Integer.parseInt(magnitudeField.getText()));
        //magnitudeField.setText("3");
        tagEntryField.requestFocus();
        tagEntryField.selectAll(); // for easy clearing
    }

    public void onClearAll() {
        for (Map.Entry<String,Integer> tag : getTagsOfSelectedTask().entrySet()) {
            taskSelector.getSelection().removeTag(tag.getKey());
        }
        tagPane.getChildren().clear();
    }

    public void onMagnitudeSelect() {
        magnitudeField.selectAll();
    }

    // getter for tags that removes special retroactive tag
    private Map<String,Integer> getTagsOfSelectedTask() {
        Activity task = taskSelector.getSelection();
        Map<String,Integer> tags = new HashMap<>();
        if (task!=null) {
            System.out.println("getting tags of task: " + task.toString());
            tags = task.getTags();
        }

        tags.remove("RETROACTIVE");
        System.out.println("tags are: " + tags.toString());
        return tags;
    }

    public synchronized void updateTaskSelector() {
        System.out.println("Updating tagging task selector");
        taskSelector.update();
    }

    public synchronized void updateTagButtons() {
        System.out.println("Updating tag buttons");
        tagPane.getChildren().clear();
        for (Map.Entry<String,Integer> tag : getTagsOfSelectedTask().entrySet()) {
            addTagButton(tag.getKey(), tag.getValue());
        }
    }

    // just for view updating purposes
    private Button addTagButton(String name, int magnitude) {
        Button tagButton = new Button(name + ": " + magnitude);
        tagButton.setMnemonicParsing(false); // turn off a stupid name mangling thing
        tagButton.setOnAction(event -> {
            String name1 = ((Button)event.getSource()).getText().split(":")[0];
            tagPane.getChildren().remove(event.getSource());
            taskSelector.getSelection().removeTag(name1);
        });
        tagPane.getChildren().add(tagButton);
        return tagButton; // prob unnecessary
    }

    private void addTag(String name, int magnitude) {
        taskSelector.getSelection().addTag(name, magnitude);
        updateTagButtons();
    }
}

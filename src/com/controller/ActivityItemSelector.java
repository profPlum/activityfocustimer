package com.controller;

import com.model.Activity;
import com.model.Model;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;

import java.util.ArrayList;

public class ActivityItemSelector {
    private ComboBox<ActivityItem> activityComboBox;
    private int maxActivities;
    private CheckBox showExtensionsBox;

    public class ActivityItem {
        public Activity act;

        ActivityItem(Activity act) {
            this.act = act;
        }

        public String toString() {
            if (act == null) return "None";
            else return act.getRelId() + " " + act.name;
        }
    }

    public ActivityItemSelector(ComboBox<ActivityItem> activityComboBox, CheckBox showExtensionsBox, int maxActivities) {
        this.activityComboBox = activityComboBox;
        this.maxActivities = maxActivities;
        this.showExtensionsBox = showExtensionsBox;
        update();
    }

    public ActivityItemSelector(ComboBox<ActivityItem> activityComboBox, int maxActivities) {
        this(activityComboBox, null, maxActivities);
    }

    public synchronized void setDisable(boolean disable) {
        // it might be best not to do this because we've learned from
        // vitamin R that the last used setting is usually the best default
        /*if (disable)
            activityComboBox.getSelectionModel().select(null);
            // don't use activityComboBox.getSelectionModel().clearSelection() or activityComboBox.getItems().clear()! they don't work!
        */

        activityComboBox.setDisable(disable);
    }

    public synchronized void update() {
        System.out.println("Updating activity item selector.");
        //assert(Model.get().getRecentActivities(maxActivities,true).containsAll(Model.get().getRecentActivities(maxActivities,false))); //TODO: delete
        assert(activityComboBox.getItems()!=null);
        ActivityItem selectedActivity = activityComboBox.getSelectionModel().getSelectedItem();
        activityComboBox.getItems().clear();

        // get recent activities as activity items (for display)
        ArrayList<ActivityItem> activityItems = new ArrayList<>();

        for (Activity act : Model.get().getRecentActivities(maxActivities, showExtensionsBox == null ? false : showExtensionsBox.isSelected())) {
            ActivityItem item_i = new ActivityItem(act);
            activityItems.add(item_i);

            // we need to update selected activity to be from new crop
            if (selectedActivity!=null && selectedActivity.act.equals(act)) {
                selectedActivity = item_i;
            }
        }
        activityComboBox.getItems().addAll(activityItems);
        if (!activityComboBox.isDisabled()) {
            if (selectedActivity!=null)
                activityComboBox.getSelectionModel().select(selectedActivity);
            else
                activityComboBox.getSelectionModel().selectFirst();
        }
    }

    public synchronized Activity getSelection() {
        ActivityItem selected = activityComboBox.getSelectionModel().getSelectedItem();
        return selected == null ? null : selected.act;
    }
}

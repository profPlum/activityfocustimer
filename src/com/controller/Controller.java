package com.controller;

import com.Main;
import com.model.Activity;
import com.model.ActivityTimer;
import com.model.IdleTimer;
import com.model.Model;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ResourceBundle;

/**
 * controller acts as middle man between view & model:
 * GUI event handler & updater
 * model accessor & manipulator
 */
public class Controller implements Initializable,
        ActivityTimer.ProgressWatcher, IdleTimer.IdleWatcher {
    // gui elements
    public SplitPane MainWindow;
    public TextField taskNameField;
    public TextField numRemindersField;
    public TextField perXMinutesField;
    public TextField minuteDurationField;
    public Button startButton;
    public Button cancelButton;
    public Button applyTagsButton;
    public ProgressBar progressBar;
    public Label timeRemainingLabel;
    public ComboBox<String> startModeSelector;
    public ComboBox<ActivityItemSelector.ActivityItem> parentActivityComboBox;
    public CheckBox retroactiveStartCheckBox;
    public Button noteWindowButton;

    private ActivityItemSelector parentSelector;

    Stage taggingWindow; // held here for opening and closing
    Stage noteWindow;

    Model model;
    final Alarm normalAlarm;
    final Alarm finalAlarm;

    public Controller(String DBpath) {
        model = Model.init(this, DBpath);
        System.out.println("Model created: " + model.toString());
        normalAlarm = new Alarm("templeBell.wav");
        finalAlarm = new Alarm("chinese-gong-daniel_simon.wav");
    }

    public Controller() {
        this("taskDB.db");
//        this(null);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            initialize();
        } catch (Throwable e) {
            Controller.handleError(e);
        }
    }
    public void initialize() {
        startModeSelector.getSelectionModel().select(0);
        startModeSelector.setDisable(true); // first task needs to be new

        parentSelector = new ActivityItemSelector(parentActivityComboBox,10);
        parentSelector.setDisable(true); // first parent needs to be none (the default)
        applyTagsButton.setDisable(true);
        retroactiveStartCheckBox.setDisable(true);
        cancelButton.setDisable(true);

        // here we make an event handler for onFocus of main window
        // that selects taskNameField for easy clearing, since this often is changed
        MainWindow.focusedProperty().addListener((obs, oldVal, newVal) -> {
            if (newVal) { // if focused
                taskNameField.requestFocus();
                taskNameField.selectAll(); // for easy clearing
            }
        });
    }

    public static void handleError(Throwable err) {
        handleError(err, "");
    }

    public static void handleError(Throwable err, String errMsg) {
        err.printStackTrace(); // print to stderr
        if (!errMsg.isEmpty()) System.err.println("error msg: " + errMsg);

        // this will automatically call `System.exit(1);` once it closes
        Platform.runLater(new Notification.ErrorNotification(err, errMsg));
    }

    public static void handleWarning(String msg) {
        System.err.println("warning: " + msg);
    }

    // NOTE: we need to use Platform.runLater() & Runnables because
    // JavaFX doesn't support UI asynchronous operations from non-main threads
    public void handleProgressUpdateEvent(float progress, boolean notifyUser) {
        if (notifyUser) {
            Alarm alarm = progress < 1.0 ? normalAlarm : finalAlarm;
            Platform.runLater(new Notification.ProgressNotification(alarm, model.getCurrentActivity(), progress));
            Platform.runLater(() -> taskNameField.selectAll());
            System.out.format("activity is: %f%% complete\n", progress * 100);
        }

        // update progress bar, 'digital clock'
        Platform.runLater(() -> {
            synchronized (this) {
                if (progress >= 1.0) {
                    cancelButton.setDisable(true);
                }

                String timeRemaining = Notification.ProgressNotification.getTimeRemainingStr(
                        model.getCurrentActivity(), progress);
                String timeLabelText = "Time Remaining: " + timeRemaining;
                timeRemainingLabel.setText(timeLabelText);
                progressBar.setProgress(progress);
            }
        });
    }

    public void handleIdleEvent(float secondsIdle, int numReminders) {
        System.out.println(String.format("Hey you've been idle for %f seconds!", secondsIdle));
        Platform.runLater(new Notification.IdleNotification(finalAlarm, secondsIdle));
        Platform.runLater(() -> taskNameField.selectAll());
    }

    // event handler mostly for button clicks
    public synchronized void onUIEvent(Event actionEvent) throws IOException {
        Object eventSource = actionEvent.getSource();

        try { // need to catch null ptr & like at entry points
            if (eventSource == startButton) {
                handleStartButton();
            } else if (eventSource == startModeSelector) {
                handleStartModeSelector();
            } else if (eventSource == parentActivityComboBox) {
                if (getStartMode().equals("Extension")) {
                    Activity parent = parentSelector.getSelection();
                    taskNameField.setText(parent == null ? "" : parent.name);
                }
            } else if (eventSource == applyTagsButton) {
                taggingWindow = handleWindowOpen("taggingWindow.fxml", "Tagging Window");
            } else if (eventSource == cancelButton) {
                model.cancelCurrentTask();
                cancelButton.setDisable(true);
//                handleProgressUpdateEvent(1.0f, false);
            } else if (eventSource == noteWindowButton) {
                noteWindow = handleWindowOpen("noteTakingWindow.fxml", "Note Taking Window");
            }/* else if (eventSource == taskNameField) {
                taskNameField.selectAll();
            }*/
        } catch (Throwable e) {
            Controller.handleError(e);
        }
    }

    private Stage handleWindowOpen(String resourceFileName, String title) throws java.io.IOException {
        Stage window = new Stage();
        URL resource = getClass().getClassLoader().getResource(resourceFileName);
        Parent taggingView = FXMLLoader.load(resource);

        window.setScene(new Scene(taggingView));
        window.setTitle(title);
        window.show();
        return window;
    }

    private void handleStartButton() {
        System.out.println("startmode: " + getStartMode());

        taskNameField.requestFocus();
        taskNameField.selectAll(); // for easy clearing

        Activity parent = parentSelector.getSelection();

        LocalDateTime startTime;
        if (retroactiveStartCheckBox.isSelected()) {
            // retroactive children should start immediately with parent
            if (parent!=null && parent.isActive()) startTime=parent.getStartTime();
            else {
                model.endCurrentTask(true);
                startTime = model.getCurrentActivity().getActualEndTime();
            }
            // a finished activity is still 'current' until it is replaced (just not 'active')
        }
        else startTime = LocalDateTime.now();

        Activity task = new Activity(taskNameField.getText(), getMinuteDuration(),
                getNumReminders(), getStartMode(), startTime);
        if (retroactiveStartCheckBox.isSelected()) {
            int magnitude = (int)(startTime.until(LocalDateTime.now(), ChronoUnit.MINUTES)/getMinuteDuration()+0.5);
            task.addTag("RETROACTIVE",magnitude);
        }

        model.endCurrentTask(true, startTime);

        task.setParent(parent);
        model.startTask(task);
        parentSelector.update();
        startModeSelector.setDisable(false);
        applyTagsButton.setDisable(false);
        retroactiveStartCheckBox.setDisable(false);
        cancelButton.setDisable(false);
    }

    public void updateRetroStartTooltip() {
        try { // need to catch null ptr & like at entry points
            Activity task = model.getCurrentActivity();
            LocalDateTime currentEndTime = task.getActualEndTime();
            if (currentEndTime == null)
                currentEndTime = LocalDateTime.now();
            long seconds = ChronoUnit.MINUTES.between(currentEndTime, LocalDateTime.now());
            Tooltip retroStartToolTip = new Tooltip(String.format("%d:%02dm ago, after \"%s\"",
                    seconds / 60, seconds % 60, task.name));
            retroactiveStartCheckBox.setTooltip(retroStartToolTip);
        } catch (Throwable e) {
            Controller.handleError(e);
        }
    }
    private void handleStartModeSelector() {
        if (getStartMode().equals("New")) {
            parentSelector.setDisable(true);
        } else {
            parentSelector.setDisable(false);
            parentSelector.update();
        }

        // if we are doing an extension it needs to have the same name
        if (getStartMode().equals("Extension")) {
            Activity parent = parentSelector.getSelection();
            taskNameField.setText(parent == null ? "" : parent.name);
            taskNameField.setDisable(true);
        } else if (taskNameField.isDisable()) {
            taskNameField.setDisable(false);
            taskNameField.requestFocus();
            taskNameField.selectAll(); // for easy clearing
        }
        System.out.println("mode event: " + getStartMode());
    }

    // basic input field getters...

    private String getStartMode() {
        String selected = startModeSelector.getSelectionModel().getSelectedItem();
        return selected == null ? "New" : selected;
    }

    private float getMinuteDuration() {
        return Float.parseFloat(minuteDurationField.getText());
    }

    private float getPerXMinutes() {
        String perXMinutes = perXMinutesField.getText();
        if (perXMinutes.isEmpty()) return getMinuteDuration();
        else return Float.parseFloat(perXMinutes);
    }

    // gets total reminders
    private int getNumReminders() {
        int numReminders = Integer.parseInt(numRemindersField.getText());
        return (int) ((numReminders / (float) getPerXMinutes()) * getMinuteDuration());
    }
}

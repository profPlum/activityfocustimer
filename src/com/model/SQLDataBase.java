package com.model;

import com.controller.Controller;

import java.sql.*;
import java.time.LocalDateTime;
// import org.sqlite.JDBC; // doesn't statically link (maybe we need to use it directly?)

// wrapper to simplify the interface & supply useful helper functions
public class SQLDataBase {
    private Connection con;
    private Statement stmnt;
    private String url;

    public synchronized void reconnect() throws SQLException {
        if (this.con!=null) {
            this.con.close();
            System.out.println("Reconnecting to DB");
        }
        try {
            Class.forName("org.sqlite.JDBC");
            this.con = DriverManager.getConnection(url);
            this.stmnt = con.createStatement();
        } catch (ClassNotFoundException e) {
            Controller.handleError(e);
        }
    }

    public SQLDataBase(String DBFilePath) throws SQLException {
        this.url = "jdbc:sqlite:" + DBFilePath;
        reconnect(); // also can just plain connect
    }

    // result set to string
    public static String RStoString(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int ncols = md.getColumnCount();
        StringBuilder print_string = new StringBuilder();
        do {
            for (int i = 1; i <= ncols; i++) {
                if (i > 1) {
                    print_string.append(", ");
                }
                String ojbString;
                try {
                    ojbString = rs.getObject(i).toString();
                } catch (NullPointerException e) {
                    ojbString = "NULL";
                }

                print_string.append(md.getColumnLabel(i) + ": " + ojbString);
            }
            print_string.append("\n");
        } while (rs.next());
        return print_string.toString();
    }

    public static void printRS(ResultSet rs) throws SQLException {
        System.out.println(RStoString(rs));
    }

    public static String getTimeSQLString(LocalDateTime dateTime) {
        // datetime format: YYYY-MM-DD HH:MM:SS
        return String.format("%02d:%02d:%02d", dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond());
    }

    public static String getDateSQLString(LocalDateTime dateTime) {
        // datetime format: YYYY-MM-DD HH:MM:SS
        return String.format("%04d-%02d-%02d", dateTime.getYear(), dateTime.getMonthValue(), dateTime.getDayOfMonth());
    }

    public static String getDateTimeSQLString(LocalDateTime dateTime) {
        // datetime format: YYYY-MM-DD HH:MM:SS
        return getDateSQLString(dateTime) + " " + getTimeSQLString(dateTime);
    }

    public synchronized ResultSet executeQuery(String sql) throws SQLException {
        System.out.printf("sql query: %s\n", sql);
        return stmnt.executeQuery(sql);
    }

    public synchronized void executeUpdate(String sql) throws SQLException {
        System.out.printf("sql update: %s\n", sql);
        stmnt.executeUpdate(sql);
    }

    public static String escapeSQLString(String input) {
        input = input.replace("\\", "\\\\");
        input = input.replace("%", "\\%");
        input = input.replace("[", "\\[");
        input = input.replace("]", "\\]");
        input = input.replace("_", "\\_");
        input=input.replace("\"","\\\"");
        return input;
    }
}

package com.model;

import com.controller.Controller;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;

// TODO: rule of 3, refactor once you have 3 timers
// TODO: use composition instead of inheritance (run isn't meant to be exposed)
public class IdleTimer extends TimerTask {
    private int updatesPassed = 0;
    private int remindersPassed = 0;
    private float secUpdateInterval;
    private float secReminderInterval;
    private Timer timer;
    private LocalDateTime startTime;

    private int getUpdateReminderInterval() {
        // we round to deal with floating point errors
        return (int)(0.5 + secReminderInterval / secUpdateInterval);
    }

    public interface IdleWatcher {
        void handleIdleEvent(float secondsPast, int remindersPassed);
    }

    // needs to be set manually
    public static IdleWatcher idleWatcher;

    /**
     * @param minReminderInterval reminder interval in minutes
     * @param secUpdateInterval update interval in seconds
     */
    public IdleTimer(float minReminderInterval, float secUpdateInterval) {
        this.secUpdateInterval = secUpdateInterval;
        this.secReminderInterval = minReminderInterval * 60;

        start();
    }
    public IdleTimer(float minReminderInterval) {
        // caused strange behaviour when debugging because 2 wasn't fast enough
        this(minReminderInterval, minReminderInterval * 60);
    }

    // TimerTask run (i.e. send progressWatcher a progress update event)
    public final void run() {
        try { // need to catch null ptr & like at entry points
            synchronized (this) {
                int remindersNeeded = ++updatesPassed / getUpdateReminderInterval();
                // ^ num reminders that should have passed
                // given current progress

                if (remindersPassed < remindersNeeded) {
                    synchronized (idleWatcher) {
                        idleWatcher.handleIdleEvent(getSecondsPast(), remindersPassed++);
                    }
                }
            }
        } catch (Throwable e) {
            Controller.handleError(e);
        }
    }

    // maybe put in constructor
    private void start() {
        // in miliseconds
        long interval = (long)(secUpdateInterval * 1000);
        synchronized (this) {
            startTime = LocalDateTime.now();
            timer = new Timer();
            timer.schedule(this, interval, interval);
        }
        System.out.println("IdleTimer started: " + toString());
    }

    /**
     * ends the timer
     * @return minutes past
     */
    public float end() {
        synchronized (this) {
            if (timer != null) timer.cancel();
            return getMinsPast();
        }
    }

    // returns float because time is always a float,
    // but not necessarily accurate beyond integer precision
    public float getSecondsPast() {
        synchronized (this) {
            return ChronoUnit.SECONDS.between(startTime, LocalDateTime.now());
        }
    }

    public float getMinsPast() {
        synchronized (this) {
            return getSecondsPast() / 60;
        }
    }
}

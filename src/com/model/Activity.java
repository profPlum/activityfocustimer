package com.model;

import com.controller.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

//TODO: use at some point
//enum ActivityType {New, Extension, SubTask};

public class Activity {
    // possibly move numReminders to a start() method
    public Activity(String taskName, float plannedDuration, int numReminders, String aType,
                    LocalDateTime startTime) {
        activityTimer = new ActivityTimer(plannedDuration, numReminders, startTime);
        this.name = taskName;
        this.aType = aType;
        isActive = true;

        try {
            System.out.println("Activity created: " + toString());
        } catch (Exception e) {
            Controller.handleError(e);
        }
    }

    public Activity(String taskName, float minuteDuration, int numReminders, String aType) {
        this(taskName, minuteDuration, numReminders, aType, LocalDateTime.now());
    }

    public void start() {
        activityTimer.start();
    }

    // set the static SQL database for recording activities & tags
    public static void setSQL_DB(SQLDataBase db) {
        Activity.db = db;
        try {
            // do initial db ops
            loadTagIdMap(db);
            ResultSet rs = db.executeQuery("SELECT MAX(aid) FROM Activities");
            idOffset = rs.getInt(1);
            System.out.format("Activity.idOffset = %d\n", idOffset);
            assert idOffset != null;
        } catch (SQLException e) {
            Controller.handleError(e);
        }
    }

    public Map<String,Integer> getTags() {
        //return new HashSet<>(tags); // safer but slower
        return tags;
    }

    public void addTag(String tag, int magnitude) {
        if (tags.containsKey(tag))
            removeTag(tag);
        synchronized (this) {
            if (recorded) {
                try {
                    String format = "INSERT INTO Tagged (aid, tid, magnitude) VALUES (%d, %d, %d)";
                    String sql = String.format(format, getId(), getTagId(tag), magnitude);
                    db.executeUpdate(sql);
                } catch (SQLException e) {
                    Controller.handleError(e);
                }
            } // else these changes will be recorded to DB after activity (so aid is valid)
            tags.put(tag,magnitude);
            System.out.printf("added tag: '%s' to: %s\n", tag, this.toString());
        }
    }

    public void removeTag(String tag) {
        synchronized (this) {
            if (recorded) {
                try {
                    String format = "DELETE FROM Tagged WHERE aid=%d AND tid=%d";
                    String sql = String.format(format, getId(), getTagId(tag));
                    db.executeUpdate(sql);
                } catch (SQLException e) {
                    Controller.handleError(e);
                }
            } // else these changes will be recorded to DB after activity (so aid is valid)
            tags.remove(tag);
            System.out.printf("removed tag: '%s' from: %s\n", tag, this.toString());
        }
    }

    private Activity parent = null; // id's are only for the DB
    private float minsEarly;
    public final String name;
    public final String aType;
    //    private ActivityType aType;

    private int relId = nextRelId++;
    private ActivityTimer activityTimer;
    private boolean isActive; // somewhat redundant with `recorded`
    private Map<String, Integer> tags = new HashMap<>(); // int is magnitude
    private boolean recorded = false;
    // can only record tags after activity is recorded (so aid is valid)

    // id's are internally stored relative to
    // the idOffset which is the greatest id existing
    // in the DB b4 this session
    static Integer idOffset = null;
    static int nextRelId = 1;

    private static SQLDataBase db = null;
    private static Map<String, Integer> tagIdMap = null;
    private static int nextTid;

    private static void loadTagIdMap(SQLDataBase db) throws SQLException {
        tagIdMap = new HashMap<>();
        ResultSet rs = db.executeQuery("SELECT tid, name FROM Tags");
        nextTid = 1;
        while (rs.next()) {
            tagIdMap.put(rs.getString("name"), rs.getInt("tid"));
            nextTid = Integer.max(nextTid, rs.getInt("tid") + 1);
            // only +1 for existing tid because this makes it a 'next'
        }
        rs.close();
    }

    // converts string tags into Ids & adds new tags into DB if necessary
    private static Integer getTagId(String tag) throws SQLException {
        Integer tid = tagIdMap.get(tag);
        if (tid == null) {
            tid = nextTid++;

            // record new tag into database
            String sql = String.format("INSERT INTO Tags (tid, name) VALUES (%d, \"%s\")", tid, tag);
            db.executeUpdate(sql);
            tagIdMap.put(tag, tid);
        }
        return tid;
    }

    // ID only has meaning in the DB
    // in code the 'id' is the memory address/reference
    private synchronized int getId() {
        return relId + idOffset;
    }

    public synchronized int getRelId() {
        return relId;
    }

    public synchronized float getPlannedMinuteDuration() {
        return activityTimer.getPlannedDuration();
    }

    // actual vs planned (which timer's getEndTime() will give you)
    // :return: null if still active, otherwise datetime
    public synchronized LocalDateTime getActualEndTime() {
        if (!isActive())
            return getStartTime().plusSeconds((long)((getPlannedMinuteDuration() - getMinsEarly())*60));
        else return null;
    }

    public synchronized int getTotalReminders() {
        return activityTimer.getTotalReminders();
    }

    @Override
    public synchronized String toString() {
        //String endStr = end == null ? "null" : end.toString();
        String fmt = "Activity(aid=%d, name=%s, start=%s, plannedMins=%f, reminders=%d)";
        return String.format(fmt, getId(), name, getStartTime().toString(),
                getPlannedMinuteDuration(), getTotalReminders());
    }

    public synchronized boolean end(LocalDateTime endTime) {
        if (!isActive()) {
            Controller.handleWarning("returning from end(), activity already ended: " + this.toString());
            return false;
        }
        this.minsEarly = activityTimer.end(endTime);
        isActive = false;
        System.out.println("Activity ended: " + toString());
        return true;
    }
//    public synchronized boolean end() {
//        return end(LocalDateTime.now());
//    }
//
//    public synchronized boolean recordAndEnd() throws SQLException {
//        return recordAndEnd(LocalDateTime.now());
//    }

    public synchronized boolean isActive() {
        return isActive;
    }

    public synchronized boolean recordAndEnd(LocalDateTime endTime) throws SQLException {
        // NOTE: this would've been cleaner to implement with exceptions...
        if (!end(endTime)) { // this is important because we don't want to record it twice...
            return false;
        }

        String parentId = getParent() == null ? "NULL" : Integer.toString(getParent().getId());

        String fields = "aid, name, start, plannedMins, minsEarly, type, pid, reminders";
        String sql = "INSERT INTO Activities (%s) VALUES (%d, '%s', '%s', %f, %f, '%s', %s, %d)";
        sql = String.format(sql, fields, getId(), name, db.getDateTimeSQLString(getStartTime()),
                getPlannedMinuteDuration(), getMinsEarly(), aType, parentId, getTotalReminders());
        try {
            db.executeUpdate(sql);
        } catch (SQLException e) {
            // workaround for mysterious crashing on recording tasks
            // not yet sure if it will work...
            Controller.handleWarning(e.toString());
            Controller.handleWarning("attempting to reconnect to DB & try again");
            db.reconnect();
            db.executeUpdate(sql);
            System.out.println("Success! Reconnecting works!");
        }

        // record activity's queued tags
        recorded = true;
        Map<String,Integer> queuedTags = tags;
        tags = new HashMap<>();
        for (Map.Entry<String,Integer> tag : queuedTags.entrySet()) {
            addTag(tag.getKey(),tag.getValue());
        }

        System.out.printf("activity recorded: %s\n", toString());
        return true;
    }

    public synchronized LocalDateTime getStartTime() {
        return activityTimer.startTime;
    }

    public synchronized Activity getParent() {
        return parent;
    }

    public synchronized void setParent(Activity parent) {
        assert(parent==null || !aType.equals("New"));
        this.parent = parent;
    }

    public synchronized float getMinsEarly() {
        return minsEarly;
    }
}

package com.model;

import com.controller.Controller;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;


// this is a "Timer for user tasks", while TimerTask is a "Activity for Java timers"
// Note: timer is one time use, starts when constructed
public class ActivityTimer extends TimerTask {
    private final int totalReminders;
    public final LocalDateTime startTime;
    private LocalDateTime endTime = null;
    private float plannedDuration;
    private int remindersPassed = 0;
    private Timer timer;
    private final float secUpdateInterval;

    private boolean isFinished() {
        return endTime!=null;
    }

    public interface ProgressWatcher {
        void handleProgressUpdateEvent(float progress, boolean notifyUser);
    }

    // needs to be set manually
    public static ProgressWatcher progressWatcher;

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public float getPlannedDuration() {
        return plannedDuration;
    }

    public int getTotalReminders() {
        return totalReminders;
    }

    public ActivityTimer(float plannedDuration, int totalReminders, LocalDateTime startTime,
                         float secUpdateInterval) {
        this.plannedDuration = plannedDuration;
        this.totalReminders = totalReminders;
        this.startTime = startTime;
        this.secUpdateInterval = secUpdateInterval;
    }

    public ActivityTimer(float minuteDuration, int totalReminders, LocalDateTime startTime) {
        this(minuteDuration, totalReminders, startTime, 3);
    }

    // IMPORTANT: it needs a separate start() method! just starting at
    // construction caused a race condition
    public synchronized void start() {
        // start the timer...
        // interval in miliseconds
        long interval = (long) (secUpdateInterval * 1000);
        timer = new Timer();
        timer.schedule(this, 0, interval);
        System.out.println("ActivityTimer started: " + toString());
    }

    // TimerTask run (i.e. send progressWatcher a progress update event)
    public final void run() {
        synchronized (this) {
            float progress = 1.0f - getMinsRemaining() / plannedDuration; // activity progress fraction
            float reminderProgressInterval = 1 / (float) totalReminders; // interval for reminders (measured in progress)
            int remindersNeeded = (int) (progress / reminderProgressInterval);
            // ^ num reminders that should have passed
            // given current progress

            boolean notifyUser = false;
            if (remindersPassed < remindersNeeded) {
                notifyUser = true;
                remindersPassed = remindersNeeded;
                // this instead of ++ avoids 'catchup reminders' for retroactive starts
            }
            synchronized (progressWatcher) {
                progressWatcher.handleProgressUpdateEvent(progress, notifyUser);
            }

            // if this is the final update then end the timer
            // this is sort'f redundant because model does this aswell...
            if (progress >= 1.0) {
                end();
            }
        }
    }

    /**
     * ends the timer
     * @return minutes remaining
     */
    public float end(LocalDateTime endTime) {
        assert(!isFinished());
        this.endTime = endTime; // this is where end time is stored
        if (timer != null) timer.cancel(); // cancel the activityTimer
        return getMinsRemaining();
    }

    public float end() {
        return end(LocalDateTime.now());
    }

    public float getMinsRemaining() {
        float misElapsed = startTime.until(LocalDateTime.now(), ChronoUnit.SECONDS) / 60f;
        float minsRemaining = plannedDuration - misElapsed;
        return Float.max(minsRemaining, 0f);
    }
}

package com.model;

import com.controller.Controller;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.min;

/**
 * Model of course acts as the 'main module' of the program,
 * abstracted from GUI & whatnot
 */
public class Model implements ActivityTimer.ProgressWatcher {
    private Controller controller;

    // stays sorted based on recency (including last time it was used as a parent)
    // extensions aren't stored because they can't be used as parents...
    private List<Activity> recentActivities;

    private Activity currentActivity;
    private SQLDataBase dataBase;
    private IdleTimer idleTimer;

    private static final float minIdleReminderInterval = 4f;
    private static final int maxRecentActivities = 30; // TODO: fix bug where this - 1 is used
    // ^ this is just to save memory, number is arbitrary

    private static final Model model = new Model();

    private Model() { }

    public static Model init(Controller controller, String DBPath) {

        // we need to end the task when time is up
        ActivityTimer.progressWatcher = get();
        IdleTimer.idleWatcher = controller;
        model.controller = controller;

        model.recentActivities = new ArrayList<>();
        model.idleTimer = new IdleTimer(minIdleReminderInterval);

        try {
            model.dataBase = new SQLDataBase(DBPath);
            Activity.setSQL_DB(model.dataBase);
            assert model.dataBase != null;
        } catch (Exception e) {
            Controller.handleError(e);
        }
        return model;
    }

    public static Model get() {
        return model;
    }
    public SQLDataBase getDataBase() {
        return dataBase;
    }

    /**
     * like linux bash cmd, updates the recency to now
     * (used when an activity is used as a parent)
     */
    private void touchRecentActivity(Activity act) {
        recentActivities.remove(act);
        recentActivities.add(0, act);
    }

    public synchronized void startTask(Activity activity) {
        assert !activity.isActive();

        endCurrentTask(true);
        currentActivity = activity;
        if (idleTimer != null) idleTimer.end();

        // put current activity in front (if it's not just an extension)
        recentActivities.add(0, currentActivity);

        while (recentActivities.size() > maxRecentActivities) // remove oldest
            recentActivities.remove(maxRecentActivities);

        // actually start the task
        currentActivity.start();
    }

    public synchronized void cancelCurrentTask() {
        System.out.printf("cancelling current activity: %s\n", currentActivity.toString());

        // NOTE: this would've been cleaner to implement with exceptions...
        // (bubbling from endCurrentTask())
        if (!isThereAnActiveTask()) {
            Controller.handleWarning("there is no active task, returning from cancelCurrentTask()");
            return;
        }
        endCurrentTask(false);
        assert recentActivities.get(0) == currentActivity;
        recentActivities.remove(currentActivity); // this activity never happened...

        // set back to previous activity
        currentActivity = !recentActivities.isEmpty() ? recentActivities.get(0) : null;
        if (recentActivities.isEmpty()) {
            controller.initialize(); // reset GUI to protect from undefined states
        }
    }

    public List<Activity> getRecentActivities(int howMany, boolean includeExtensions) {
        synchronized (this) {
            List<Activity> tmp_activities = null;
            try {
                // NOTE: could use LinkedList
                tmp_activities = new ArrayList<>(recentActivities);

                // NOTE: can't iterate over tmp_activites while modifying it!
                for (Activity act : recentActivities) {
                    if (!includeExtensions && act.aType.equals("Extension")) {
                        tmp_activities.remove(act);
                    }
                }
                howMany = min(howMany, tmp_activities.size());
            } catch (Throwable e) {
                Controller.handleError(e);
            }
            return tmp_activities.subList(0, howMany);
        }
    }

    public Activity getCurrentActivity() {
        synchronized (this) {
            return currentActivity;
        }
    }

    public boolean isThereAnActiveTask() {
        synchronized (this) {
            return currentActivity == null ? false : currentActivity.isActive();
        }
    }

    public void endCurrentTask(boolean record) {
        endCurrentTask(record,LocalDateTime.now());
    }

    public synchronized void endCurrentTask(boolean record, LocalDateTime endTime) {

        System.out.println("entering endCurrentTask()");
            if (!isThereAnActiveTask()) {
                Controller.handleWarning("there is no active task, returning from endCurrentTask()");
                return;
            }

            if (record) {
                // recording means task finished (vs being cancelled) so we touch parent
                if (currentActivity.getParent() != null) {
                    touchRecentActivity(currentActivity.getParent());
                    touchRecentActivity(currentActivity);
                }

                try {
                    currentActivity.recordAndEnd(endTime);
                } catch (SQLException e) {
                    Controller.handleError(e);
                }
            } else currentActivity.end(endTime);
            idleTimer = new IdleTimer(minIdleReminderInterval);
    }

    // from recent activities
    public synchronized List<Activity> findChildren(Activity parent) {
        List<Activity> children = new ArrayList<>();
        for (Activity act_i : recentActivities) {
            if (act_i.getParent() == parent) children.add(act_i);
        }
        return children;
    }

    public void handleProgressUpdateEvent(float progress, boolean notifyUser) {
        if (progress >= 1.0) {
            endCurrentTask(true);
        }
        controller.handleProgressUpdateEvent(progress, notifyUser);
    }
}

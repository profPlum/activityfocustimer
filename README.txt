IMPORTANT: make sure the absolute directories in the project file are correct!
They will likely not be once you get a new computer or move the project...

NOTE: JFX11 works but it is really weird because it requires you statically linking all 3 native versions of inside the jar (solution here: https://stackoverflow.com/questions/53533486/how-to-open-javafx-jar-file-with-jdk-11). If you want cross platform one turn off the OSX artifact target.

Bugs:
* alarm still stops working sometimes (after a while, race condition?)
* random crashes sometimes, complains about database not being found

Regarding Rabbit Holes (important!):
* your issue is rabit wholes so you need to be able to see the task tree
so you can tell when you've gone too deep or extended a task too many times
* implement some sort of max recursive depth warning
* & some max extension warning

Regarding tagging:
* the best way to do this is to use a flowpane with *buttons*
	> the button textw ill be the tag 
	> & on button click the tag will be removed
* consider encapsulating the recent activity logic (?)

Features:
* disable 'oking' the prompt from the keyboard, I'm typing all the time & it's really annoying...
* make it easier to see the name of the task (put it in header)
* include idle time as 'extensions' of some sort
* make resume a new task type (& have user select parent)
* interface with google tasks
* cancel/finish button

SQL design options:
Regarding redundant data:
* have a second table that stores task roots (good design + simplicity & speed)
* store planned timed & actual time
    > simple and saves a lot of computational time
    > have prepared SQL statements that update the actual times to match the implied values from subtasks/extensions

* Old: (eehh not too important...) <- id is equaivalent to memmory address, so the code should only care about address...
* guarentee task id uniquenes
    > involves making my own garbage collector of sorts
    > might be more trouble than it's worth
* allow task id duplicates <- WINNER
    > less work
    > might allow weird invalid states
* have activities recorded & loaded in DAO (data access object, aka SQLDataBase)
    > could have Activity & child class ActivityTree
    > ActivityTree could support methods that allow for structural construction from DAO
    > they would both be treated like Activities though in regards to loading and recording
    
Also: https://stackoverflow.com/questions/1661921/java-programming-where-should-sql-statements-be-stored
